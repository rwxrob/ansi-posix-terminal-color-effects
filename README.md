ANSI POSIX Terminal Color Effects
=================================

Easiest way to add basic terminal color and effects to any ANSI POSIX
terminal (including REPL.it). Note that the cursor positioning escapes
(line and screen clear) are not included because they are not widely
supported on terminal emulators that are popular with cloud terminal
emulators.

These are not in module form, but could easily be made into modules
using the terms others may prefer. They are just here to capture the
values as simple constants.

Equivalents are available on repl.it:

* https://REPL.it/@robmuh/color-effects-python
* https://REPL.it/@robmuh/color-effects-nodejs
* https://REPL.it/@robmuh/color-effects-go
* https://REPL.it/@robmuh/color-effects-cpp
* https://REPL.it/@robmuh/color-effects-java


