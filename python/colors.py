#!/usr/bin/env python3

black = '\x1b[30m'
red = '\x1b[31m'
green = '\x1b[32m'
yellow = '\x1b[33m'
blue = '\x1b[34m'
magenta = '\x1b[35m'
cyan = '\x1b[36m'
white = '\x1b[37m'
defcl = '\x1b[39m'

blackbg = '\x1b[40m'
redbg = '\x1b[41m'
greenbg = '\x1b[42m'
yellowbg = '\x1b[43m'
bluebg = '\x1b[44m'
magentabg = '\x1b[45m'
cyanbg = '\x1b[46m'
whitebg = '\x1b[47m'
defbg = '\x1b[49m'

clear = '\x1b[0;0m'

bold = '\x1b[1m'
italic = '\x1b[3m'
uline = '\x1b[4m'
sblink = '\x1b[5m'
fblink = '\x1b[6m'
inverse = '\x1b[7m'
hide = '\x1b[8m'
strike = '\x1b[9m'

unbold = '\x1b[22m'
unitalic = '\x1b[23m'
unline = '\x1b[24m'
unblink = '\x1b[25m'
uninverse = '\x1b[27m'
unhide = '\x1b[28m'
unstrike = '\x1b[29m'

colors = [
    black, red, green, yellow, blue, magenta,
    cyan, white
]

backgrounds = [
    blackbg, redbg, greenbg, yellowbg, bluebg, magentabg, cyanbg, whitebg
]

print(f'{clear}')

for bg in backgrounds: 
    for color in colors:
        print(f'{bg}{color}color ', end='')
    print(f'{clear}')
       
print(f'{clear}')
       
print()
print(f'{bold}bold{unbold} normal')
print(f'{italic}italic{unitalic} normal')
print(f'{sblink}sblink{unblink} normal')
print(f'{fblink}fblink{unblink} normal')
print(f'{inverse}inverse{uninverse} normal (kinda broke)')
print(f'{hide}hide{unhide} (hide) normal')
print(f'{uline}uline{unline} normal')
print(f'{strike}strike{unstrike} normal')
