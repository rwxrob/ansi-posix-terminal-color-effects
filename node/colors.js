#!/usr/bin/env node

const black = '\x1b[30m'
const red = '\x1b[31m'
const green = '\x1b[32m'
const yellow = '\x1b[33m'
const blue = '\x1b[34m'
const magenta = '\x1b[35m'
const cyan = '\x1b[36m'
const white = '\x1b[37m'
const defcl = '\x1b[39m'

const blackbg = '\x1b[40m'
const redbg = '\x1b[41m'
const greenbg = '\x1b[42m'
const yellowbg = '\x1b[43m'
const bluebg = '\x1b[44m'
const magentabg = '\x1b[45m'
const cyanbg = '\x1b[46m'
const whitebg = '\x1b[47m'
const defbg = '\x1b[49m'

const clear = '\x1b[0;0m'

const bold = '\x1b[1m'
const italic = '\x1b[3m'
const uline = '\x1b[4m'
const sblink = '\x1b[5m'
const fblink = '\x1b[6m'
const inverse = '\x1b[7m'
const hide = '\x1b[8m'
const strike = '\x1b[9m'

const unbold = '\x1b[22m'
const unitalic = '\x1b[23m'
const unline = '\x1b[24m'
const unblink = '\x1b[25m'
const uninverse = '\x1b[27m'
const unhide = '\x1b[28m'
const unstrike = '\x1b[29m'

const colors = [
    black, red, green, yellow, blue, magenta,
    cyan, white
]

const backgrounds = [
    blackbg, redbg, greenbg, yellowbg, bluebg, magentabg, cyanbg, whitebg
]

const print = text => { process.stdout.write(text) }
const println = text => { process.stdout.write(text+'\n') }

print(`${clear}`)

for (let bg of backgrounds) {
    for (let color of colors) {
        print(`${bg}${color}color `)
    }
    println(`${clear}`)
}
       
println(`${clear}`)

println(`${bold}bold${unbold} normal`)
println(`${italic}italic${unitalic} normal`)
println(`${sblink}sblink${unblink} normal`)
println(`${fblink}fblink${unblink} normal`)
println(`${inverse}inverse${uninverse} normal (kinda broke)`)
println(`${hide}hide${unhide} (hide) normal`)
println(`${uline}uline${unline} normal`)
println(`${strike}strike${unstrike} normal`)
