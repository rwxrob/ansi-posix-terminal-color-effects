package main

import "fmt"

const (
	black   = "\x1b[30m"
	red     = "\x1b[31m"
	green   = "\x1b[32m"
	yellow  = "\x1b[33m"
	blue    = "\x1b[34m"
	magenta = "\x1b[35m"
	cyan    = "\x1b[36m"
	white   = "\x1b[37m"
	defcl   = "\x1b[39m"

	blackbg   = "\x1b[40m"
	redbg     = "\x1b[41m"
	greenbg   = "\x1b[42m"
	yellowbg  = "\x1b[43m"
	bluebg    = "\x1b[44m"
	magentabg = "\x1b[45m"
	cyanbg    = "\x1b[46m"
	whitebg   = "\x1b[47m"
	defbg     = "\x1b[49m"

	clear = "\x1b[00m"

	bold    = "\x1b[1m"
	italic  = "\x1b[3m"
	uline   = "\x1b[4m"
	sblink  = "\x1b[5m"
	fblink  = "\x1b[6m"
	inverse = "\x1b[7m"
	hide    = "\x1b[8m"
	strike  = "\x1b[9m"

	unbold    = "\x1b[22m"
	unitalic  = "\x1b[23m"
	unline    = "\x1b[24m"
	unblink   = "\x1b[25m"
	uninverse = "\x1b[27m"
	unhide    = "\x1b[28m"
	unstrike  = "\x1b[29m"
)

var colors = []string{
	black, red, green, yellow, blue, magenta, cyan, white,
}

var backgrounds = []string{
	blackbg, redbg, greenbg, yellowbg, bluebg, magentabg, cyanbg, whitebg,
}

func main() {
	var bg, c int

	fmt.Print(clear)

	for bg = 0; bg < 8; bg++ {
		for c = 0; c < 8; c++ {
			fmt.Print(backgrounds[bg] + colors[c] + "color ")
		}

		fmt.Println(clear)

	}

	fmt.Println(clear)

	fmt.Println(bold + "bold" + unbold + " normal")
	fmt.Println(italic + "italic" + unitalic + " normal")
	fmt.Println(sblink + "sblink" + unblink + " normal")
	fmt.Println(fblink + "fblink" + unblink + " normal")
	fmt.Println(inverse + "inverse" + uninverse + " normal")
	fmt.Println(hide + "hide" + unhide + " normal")
	fmt.Println(uline + "uline" + unline + " normal")
	fmt.Println(strike + "strike" + unstrike + " normal")
}
