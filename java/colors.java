import java.util.*;
  
class Main {
  
  static final String black = "\u001b[30m";
  static final String red = "\u001b[31m";
  static final String green = "\u001b[32m";
  static final String yellow = "\u001b[33m";
  static final String blue = "\u001b[34m";
  static final String magenta = "\u001b[35m";
  static final String cyan = "\u001b[36m";
  static final String white = "\u001b[37m";
  static final String defcl = "\u001b[39m";
 
  static final String blackbg = "\u001b[40m";
  static final String redbg = "\u001b[41m";
  static final String greenbg = "\u001b[42m";
  static final String yellowbg = "\u001b[43m";
  static final String bluebg = "\u001b[44m";
  static final String magentabg = "\u001b[45m";
  static final String cyanbg = "\u001b[46m";
  static final String whitebg = "\u001b[47m";
  static final String defbg = "\u001b[49m";
  
  static final String clear = "\u001b[0;0m";
  
  static final String bold = "\u001b[1m";
  static final String italic = "\u001b[3m";
  static final String uline = "\u001b[4m";
  static final String sblink = "\u001b[5m";
  static final String fblink = "\u001b[6m";
  static final String inverse = "\u001b[7m";
  static final String hide = "\u001b[8m";
  static final String strike = "\u001b[9m";
  
  static final String unbold = "\u001b[22m";
  static final String unitalic = "\u001b[23m";
  static final String unline = "\u001b[24m";
  static final String unblink = "\u001b[25m";
  static final String uninverse = "\u001b[27m";
  static final String unhide = "\u001b[28m";
  static final String unstrike = "\u001b[29m";
  
  static final List<String> colors = Arrays.asList(
      black, red, green, yellow, blue, magenta,
      cyan, white
  );
  
  static final List<String> backgrounds = Arrays.asList(
      blackbg, redbg, greenbg, yellowbg, bluebg, magentabg, cyanbg, whitebg
  );
  
  public static void main(String[] args) {
      for (String bg: backgrounds) {
          for (String color: colors) {
              System.out.print(bg + color + "color ");
          }
          System.out.println();
      }
      
      System.out.println(clear);
      
      System.out.println(bold + "bold" + unbold + " normal");  
      System.out.println(italic + "italic" + unitalic + " normal");  
      System.out.println(sblink + "sblink" + unblink + " normal");  
      System.out.println(fblink + "fblink" + unblink + " normal");  
      System.out.println(inverse + "inverse" + uninverse + " normal");  
      System.out.println(hide + "hide" + unhide + " normal");  
      System.out.println(uline + "uline" + unline + " normal");  
      System.out.println(strike + "strike" + unstrike + " normal");  
  }
}

