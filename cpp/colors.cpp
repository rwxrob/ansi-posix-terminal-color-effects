#include <iostream>
#include <string>
using namespace std;

string black = "\x1b[30m";
string red = "\x1b[31m";
string green = "\x1b[32m";
string yellow = "\x1b[33m";
string blue = "\x1b[34m";
string magenta = "\x1b[35m";
string cyan = "\x1b[36m";
string white = "\x1b[37m";
string defcl = "\x1b[39m";

string blackbg = "\x1b[40m";
string redbg = "\x1b[41m";
string greenbg = "\x1b[42m";
string yellowbg = "\x1b[43m";
string bluebg = "\x1b[44m";
string magentabg = "\x1b[45m";
string cyanbg = "\x1b[46m";
string whitebg = "\x1b[47m";
string defbg = "\x1b[49m";

string clear = "\x1b[0;0m";

string bold = "\x1b[1m";
string italic = "\x1b[3m";
string uline = "\x1b[4m";
string sblink = "\x1b[5m";
string fblink = "\x1b[6m";
string inverse = "\x1b[7m";
string hide = "\x1b[8m";
string strike = "\x1b[9m";

string unbold = "\x1b[22m";
string unitalic = "\x1b[23m";
string unline = "\x1b[24m";
string unblink = "\x1b[25m";
string uninverse = "\x1b[27m";
string unhide = "\x1b[28m";
string unstrike = "\x1b[29m";

string colors[] = {
    black, red, green, yellow, blue, magenta,
    cyan, white
};

string backgrounds[] = {
    blackbg, redbg, greenbg, yellowbg, bluebg, magentabg, cyanbg, whitebg
};

int main() {
  int bg, c;
  
  cout << clear;
  
  for (bg = 0; bg < 8; bg++)
  {
    for (c = 0; c < 8; c++)
    {
      cout << backgrounds[bg] << colors[c] << "color ";
    }
    
    cout << clear << endl;
    
  }
  
  cout << clear << endl;
  
  cout << bold << "bold" << unbold << " normal" << endl;
  cout << italic << "italic" << unitalic << " normal" << endl;
  cout << sblink << "sblink" << unblink << " normal" << endl;
  cout << fblink << "fblink" << unblink << " normal" << endl;
  cout << inverse << "inverse" << uninverse << " normal" << endl;
  cout << hide << "hide" << unhide << " normal" << endl;
  cout << uline << "uline" << unline << " normal" << endl;
  cout << strike << "strike" << unstrike << " normal" << endl;
  
}
